#include "TicTacToe.h"

TicTacToe::TicTacToe() : charX(88), charO(79), sleepTime(2500)
{	
	for (int i = 0, j = 49; i < (sizeof(charSymbols) / sizeof(charSymbols[0])); i++, j++)
		charSymbols[i] = j;

	for (int i = 0; i < (sizeof(results) / sizeof(results[0])); i++)
		results[i] = 0;

	whoseTurn = 0;
	gameOver = 0;
	playAgain = 1;

	x_Or_O_Switch = charX;

	singleOrMultiDrawing = 0;
	
	srand(time(NULL));
}

void TicTacToe::playGame()
{

	while (playAgain) // playAgain == 1
	{
		singleOrMultiDrawing = menuChoice();

		while (!gameOver) // same like gameOver == 0
		{

			if (singleOrMultiDrawing == 1)
			{
				drawBoard();
				checkMoveSingle();
				checkResult();
			}
			else if (singleOrMultiDrawing == 2)
			{
				drawBoard();
				checkMove();
				checkResult();
			}
			else if (singleOrMultiDrawing == 3)
			{
				system("cls");
				cout << endl << "Thank you for playing! Game shutting down..." << endl;
				Sleep(1500);
				playAgain = 0;
				exit(0);
			}

		}

		drawBoard();
		checkResult();
		wantPlayAgain();
	}

}

void TicTacToe::drawBoard()
{
	system("cls");

	if (singleOrMultiDrawing == 2)
		cout << "      Multiplayer mode" << endl;
	if (singleOrMultiDrawing == 1)
		cout << "      Singleplayer mode" << endl;
	cout << endl;
	cout << "\tTic Tac Toe" << endl << endl;	
	if (singleOrMultiDrawing == 2)
		cout << "Player 1 (X) vs. Player 2 (O)" << endl;
	if (singleOrMultiDrawing == 1)
		cout << "Player 1 (X) vs. Computer (O)" << endl;
	cout << endl;

	cout << "\t   |    |    " << endl;
	cout << "\t " << static_cast<char>(charSymbols[0]) << " |  " << static_cast<char>(charSymbols[1]) << " |  " << static_cast<char>(charSymbols[2]) << endl;
	cout << "\t   |    |    " << endl;
	cout << "       ______________" << endl;
	cout << "\t   |    |    " << endl;
	cout << "\t " << static_cast<char>(charSymbols[3]) << " |  " << static_cast<char>(charSymbols[4]) << " |  " << static_cast<char>(charSymbols[5]) << endl;
	cout << "\t   |    |    " << endl;
	cout << "       ______________" << endl;
	cout << "\t   |    |    " << endl;
	cout << "\t " << static_cast<char>(charSymbols[6]) << " |  " << static_cast<char>(charSymbols[7]) << " |  " << static_cast<char>(charSymbols[8]) << endl;
	cout << "\t   |    |    " << endl << endl;
}

void TicTacToe::move()
{
	x_Or_O_Switch = charX;

	if (whoseTurn == 1)
	{
		x_Or_O_Switch = charO;
	}

	switch (playerMove)
	{

		case 1:
			charSymbols[0] = x_Or_O_Switch;
			results[0] = x_Or_O_Switch;
			break;

		case 2:
			charSymbols[1] = x_Or_O_Switch;
			results[1] = x_Or_O_Switch;
			break;

		case 3:
			charSymbols[2] = x_Or_O_Switch;
			results[2] = x_Or_O_Switch;
			break;

		case 4:
			charSymbols[3] = x_Or_O_Switch;
			results[3] = x_Or_O_Switch;
			break;

		case 5:
			charSymbols[4] = x_Or_O_Switch;
			results[4] = x_Or_O_Switch;
			break;

		case 6:
			charSymbols[5] = x_Or_O_Switch;
			results[5] = x_Or_O_Switch;
			break;

		case 7:
			charSymbols[6] = x_Or_O_Switch;
			results[6] = x_Or_O_Switch;
			break;

		case 8:
			charSymbols[7] = x_Or_O_Switch;
			results[7] = x_Or_O_Switch;
			break;

		case 9:
			charSymbols[8] = x_Or_O_Switch;
			results[8] = x_Or_O_Switch;
			break;

	}
}

void TicTacToe::checkResult()
{
	if (((results[0] == charX) && (results[3] == charX) && (results[6] == charX)) || ((results[1] == charX) && (results[4] == charX) && (results[7] == charX)) || ((results[2] == charX) && (results[5] == charX) && (results[8] == charX)) || ((results[0] == charX) && (results[1] == charX) && (results[2] == charX)) ||
		((results[3] == charX) && (results[4] == charX) && (results[5] == charX)) || ((results[6] == charX) && (results[7] == charX) && (results[8] == charX)) || ((results[0] == charX) && (results[4] == charX) && (results[8] == charX)) || ((results[2] == charX) && (results[4] == charX) && (results[6] == charX)))
	{
		cout << endl << "Player 1 (X) won!" << endl << endl;
		gameOver = 1;
	}

	else if (((results[0] == charO) && (results[3] == charO) && (results[6] == charO)) || ((results[1] == charO) && (results[4] == charO) && (results[7] == charO)) || ((results[2] == charO) && (results[5] == charO) && (results[8] == charO)) || ((results[0] == charO) && (results[1] == charO) && (results[2] == charO)) ||
		((results[3] == charO) && (results[4] == charO) && (results[5] == charO)) || ((results[6] == charO) && (results[7] == charO) && (results[8] == charO)) || ((results[0] == charO) && (results[4] == charO) && (results[8] == charO)) || ((results[2] == charO) && (results[4] == charO) && (results[6] == charO)))
	{
		if (singleOrMultiDrawing == 2)
			cout << "Player 2 (O) won!" << endl;
		if (singleOrMultiDrawing == 1)
			cout << "Computer (O) won!" << endl;

		gameOver = 1;
	}

	else if ((results[0] != 0) && (results[1] != 0) && (results[2] != 0) && (results[3] != 0) && (results[4] != 0) && (results[5] != 0) && (results[6] != 0) && (results[7] != 0) && (results[8] != 0))
	{
		cout << endl << "Draw!" << endl << endl;
		gameOver = 1;
	}
}

void TicTacToe::checkMove()
{
	string choice;

	if (whoseTurn == 0)
	{
			playerOneCheckMove();
	}

	else
	{
		cout << "Player 2 (O) choose field: ";
		cin >> choice;

		while (choice != "1" && choice != "2" && choice != "3" && choice != "4" && choice != "5" && choice != "6" && choice != "7" && choice != "8" && choice != "9")
		{
			cinClearAndIgnore();
			drawBoard();
			cout << "Invalid input! You have to repeat the move in a moment." << endl;
			Sleep(sleepTime);
			drawBoard();
			cout << "Player 2 (O) choose field: ";
			cin >> choice;
		}

		playerMove = stoi(choice);

			if ((results[playerMove - 1] == charX) || (results[playerMove - 1] == charO))
			{
				drawBoard();
				cout << "Field is already occupied! You have to repeat the move in a moment." << endl;
				whoseTurn = 1;
				Sleep(sleepTime);
			}
			else
			{
				move();
				whoseTurn = 0;
			}
	}
}

void TicTacToe::checkMoveSingle()
{

	if (whoseTurn == 0)
	{ 
		playerOneCheckMove();
	}

	else
	{
		cout << "Computer is thinking... ";

		Sleep(700);

		do
		{
			playerMove = rand() % 9 + 1;

		} while ((results[playerMove - 1] == charX) || (results[playerMove - 1] == charO));

		move();
		whoseTurn = 0;		
	}
}

void TicTacToe::wantPlayAgain()
{
	string choice;

	cout << "Do you want to come back to menu and play again?" << endl;
	cout << "Press \'Y\' for YES and \'N\' for NO: ";
	cin >> choice;

	while (choice != "Y" && choice != "y" && choice != "N" && choice != "n")
	{
		cinClearAndIgnore();
		drawBoard();
		checkResult();
		cout << "Invalid input! You have to repeat your choice in a moment." << endl;
		Sleep(sleepTime);
		drawBoard();
		checkResult();
		cout << "Do you want to come back to menu and play again?" << endl;
		cout << "Press \'Y\' for YES and \'N\' for NO : ";
		cin >> choice;
	}

	if ((choice == "N") || (choice == "n"))
	{
		system("cls");
		cout << endl << "Thank you for playing! Game shutting down..." << endl;
		Sleep(3000);
		playAgain = 0;
	}
	else if ((choice == "Y") || (choice == "y"))
	{
		
		dataDefault();

		system("cls");
		cout << "You are coming back to menu..." << endl;
		Sleep(sleepTime);
	}
}

int TicTacToe::menuChoice()
{
	string choice;

	drawMenu();
	cout << "What do you want to do? Choose proper number: ";
	cin >> choice;

	while (choice != "1" && choice != "2" && choice != "3")
	{
		cinClearAndIgnore();
		drawMenu();
		cout << "Invalid input! You have to repeat your choice in a moment." << endl;
		Sleep(sleepTime);
		drawMenu();
		cout << "What do you want to do? Choose proper number: ";
		cin >> choice;
	}

		return stoi(choice);
}

void TicTacToe::drawMenu()
{
	system("cls");

	cout << "Welcome to the game Tic Tac Toe!" << endl << endl;
	cout << "1. Play singleplayer mode (Player vs. Computer)" << endl;
	cout << "2. Play multiplayer mode (Player vs. Player)" << endl;
	cout << "3. Exit the game" << endl << endl;
}

void TicTacToe::playerOneCheckMove()
{
	string choice;

	cout << "Player 1 (X) choose field: ";
	cin >> choice;

	while (choice != "1" && choice != "2" && choice != "3" && choice != "4" && choice != "5" && choice != "6" && choice != "7" && choice != "8" && choice != "9")
	{
		cinClearAndIgnore();
		drawBoard();
		cout << "Invalid input! You have to repeat the move in a moment." << endl;
		Sleep(sleepTime);
		drawBoard();
		cout << "Player 1 (X) choose field: ";
		cin >> choice;
	}

	playerMove = stoi(choice);

	if ((results[playerMove - 1] == charX) || (results[playerMove - 1] == charO))
	{
		drawBoard();
		cout << "Field is already occupied! You have to repeat the move in a moment." << endl;
		whoseTurn = 0;
		Sleep(sleepTime);
	}
	else
	{
		move();
		whoseTurn = 1;
	}
}

void TicTacToe::dataDefault()
{
	for (int i = 0, j = 49; i < (sizeof(charSymbols) / sizeof(charSymbols[0])); i++, j++)
		charSymbols[i] = j;

	for (int i = 0; i < (sizeof(results) / sizeof(results[0])); i++)
		results[i] = 0;

	whoseTurn = 0;
	gameOver = 0;
	playAgain = 1;

	x_Or_O_Switch = charX;

	singleOrMultiDrawing = 0;
}

void TicTacToe::cinClearAndIgnore()
{
	cin.clear();
	cin.ignore(1024, '\n');
}