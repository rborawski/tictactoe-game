#pragma once
#include <iostream>
#include <Windows.h>
#include <string>
#include <cstdlib>
#include <ctime>

using namespace std;

class TicTacToe
{
private:
	int charSymbols[9]; // ASCII numbers, at the beginning 1 - 9
	int results[9]; // possible fields to choose
	const int charX; // X ASCII = 88
	const int charO; // O ASCII = 79
	const int sleepTime; // sleeping time when wrong move = 2500
	int playerMove; // field chosen by the player
	bool whoseTurn; // 0 - player 1(X),    1 - player 2(O)
	bool gameOver; // 0 - playing, 1 - end
	bool playAgain; // 0 - when exit game, 1 - when play again
	int x_Or_O_Switch; // helpful variable used in switch
	int singleOrMultiDrawing; // variable used for drawing field

public:
	TicTacToe(); // constructor
	void playGame(); // something like class main

private:
	void drawBoard(); // drawing fields
	void move(); // doing players move
	void checkResult(); // checking if someone won after every move
	void checkMove(); // checking possibility of move when multiplayer is active
	void checkMoveSingle(); // checking possibility of move when singleplayer is active
	void wantPlayAgain(); //question Yes or No if u want to open menu again and play or exit
	void drawMenu(); //drawing menu
	int menuChoice(); //menu choice 1. Single, 2. Multi, 3.Exit
	void playerOneCheckMove(); // checking possibility of player 1 move (used in checkMove() and checkMoveSingle()
	void dataDefault(); // default options
	void cinClearAndIgnore(); // cin.clear cin.ignore

};