#include <iostream>
#include "TicTacToe.h"

using namespace std;

int main()
{
	TicTacToe ticTacToe;
	TicTacToe *const p_ticTacToe = &ticTacToe;
	p_ticTacToe->playGame();

	return 0;
}